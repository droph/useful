package com.droph.useful.brms.rule;

import com.droph.useful.brms.DecisionHelper;
import com.droph.useful.brms.flow.FlowType;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DecisionTest {
    @org.junit.Rule
    public ExpectedException thrown = ExpectedException.none();

    private ResultRule<?, ?> testRule;
    private String testRuleName = "testRule";
    private FlowType testFlow = FlowType.of("test", String.class);

    @Before
    public void init() {
        testRule = DecisionHelper.getReasonRule(testRuleName, testFlow);
    }

    @Test
    public void testHolder() {
        Decision.Builder builder = Decision.builder(testRule);
        builder.prepend(testRule);
        builder.prepend(testRule);
        Decision complete = builder.build();
        assertThat(complete.getPath(), is(testRule.getName() + "/" + testRule.getName() + "/" + testRule.getName()));
        assertThat(complete.getResult(), is(testRule.getResult()));
    }
}
