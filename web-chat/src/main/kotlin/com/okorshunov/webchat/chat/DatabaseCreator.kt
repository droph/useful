package com.okorshunov.webchat.chat

import org.springframework.data.r2dbc.core.DatabaseClient

class DatabaseCreator(private val client: DatabaseClient) {
    fun initDb() {

        println("Creating database tables")

        val statements = arrayListOf("DROP TABLE IF EXISTS CHAT_MESSAGE;",
                                     "CREATE TABLE CHAT_MESSAGE ( ID SERIAL PRIMARY KEY, " +
                                             "SENT_TO VARCHAR(100) NOT NULL, " +
                                             "SENT_FROM VARCHAR(100) NOT NULL," +
                                             "MSG_BODY VARCHAR(200));")

        statements.forEach { st ->
            client.execute(st)
                    .fetch()
                    .rowsUpdated()
                    .subscribe { println("Rows updated $it") }
        }

        println("Database tables have been created")

    }
}