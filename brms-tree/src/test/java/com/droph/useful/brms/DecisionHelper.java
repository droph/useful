package com.droph.useful.brms;

import com.droph.useful.brms.flow.FlowType;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.ResultRule;
import io.vavr.control.Option;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class DecisionHelper {
    public static DecisionAware getDecisionAware(FlowType flowType) {
        return new DecisionAware() {
            Map<FlowType<?>, Decision<?>> map = new HashMap<>();

            @Override
            public <R extends Serializable> void addDecision(FlowType<R> type, Decision<R> holder) {
                map.put(type, holder);
            }

            @Override
            @SuppressWarnings("unchecked")
            public <R extends Serializable> Option<Decision<R>> getDecision(FlowType<R> type) {
                return Option.of((Decision<R>) map.get(type));
            }
        };
    }

    public static <T extends DecisionAware> ResultRule<T, TestResult> getReasonRule(String ruleName, FlowType<TestResult> flowType) {
        return flowType.reasonRuleOf(ruleName, new TestResult());
    }

    public static <T extends DecisionAware, R extends Serializable> DecisionRule<T, R> getDecisionRule(String name, Function<T, Object> decision, FlowType<R> flowType) {
        return flowType.decisionRuleOf(name, decision);

    }

    public enum TestFlowType implements FlowType<TestResult> {
        TEST_FLOW_TYPE,
        ANOTHER_TEST_FLOW_TYPE;

        @Override
        public Class<TestResult> getFlowTypeClass() {
            return TestResult.class;
        }

        @Override
        public String getName() {
            return name();
        }
    }

    public static class TestResult implements Serializable {
        public String getReasonCode() {
            return "111";
        }

        public Object getFlag() {
            return null;
        }
    }
}