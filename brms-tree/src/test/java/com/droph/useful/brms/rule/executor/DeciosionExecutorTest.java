package com.droph.useful.brms.rule.executor;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.DecisionHelper;
import com.droph.useful.brms.DecisionHelper.TestResult;
import com.droph.useful.brms.exception.IncompleteFlowException;
import com.droph.useful.brms.flow.FlowType;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.ResultRule;
import io.vavr.Tuple;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.droph.useful.brms.DecisionHelper.TestFlowType.TEST_FLOW_TYPE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DeciosionExecutorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private DecisionRule<DecisionAware, TestResult> decisionRule;
    private DecisionExecutor<DecisionAware, TestResult> decisionExecutor;
    private ResultRule<DecisionAware, TestResult> trueRule;
    private ResultRule<DecisionAware, TestResult> falseRule;
    private DecisionAware decisionAware;
    private FlowType<TestResult> testFlow = TEST_FLOW_TYPE;

    @Before
    public void init() {
        decisionRule = DecisionHelper.getDecisionRule("decisionRule", t -> false, testFlow);
        trueRule = DecisionHelper.getReasonRule("trueRule", testFlow);
        falseRule = DecisionHelper.getReasonRule("falseRule", testFlow);
        prepareDecisionExecutor();

    }

    private void prepareDecisionExecutor() {
        decisionExecutor = DecisionExecutor.of(Tuple.of(decisionRule, 0));
        decisionExecutor.addDefault(ResultExecutor.of(falseRule));
        decisionExecutor.addCase(true, ResultExecutor.of(trueRule));
    }

    @Test
    public void testOf() {
        assertThat(decisionExecutor.getRule(), is(decisionRule));
    }

    @Test
    public void testLeafs() {
        assertThat(decisionExecutor.getExecutorMap().head()._2.getRule(), is(trueRule));
        assertThat(decisionExecutor.getDefaultRuleExecutor().getRule(), is(falseRule));
    }

    @Test
    public void testExecuteTrue() {
        decisionRule = DecisionHelper.getDecisionRule("decisionRule", t -> true, testFlow);
        prepareDecisionExecutor();
        Decision<TestResult> builder = decisionExecutor.execute(decisionAware).build();
        assertThat(builder.getResult(), is(trueRule.getResult()));
    }

    @Test
    public void testExecuteFalse() {
        decisionAware = DecisionHelper.getDecisionAware(testFlow);
        Decision<TestResult> builder = decisionExecutor.execute(decisionAware).build();
        assertThat(builder.getResult(), is(falseRule.getResult()));
    }

    @Test
    public void testValidateIncomplete() {
        decisionExecutor.addDefault(null);
        thrown.expect(IncompleteFlowException.class);
        decisionExecutor.validate();
    }
}
