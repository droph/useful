package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.DecisionHelper;
import com.droph.useful.brms.DecisionHelper.TestResult;
import com.droph.useful.brms.exception.IncompleteFlowException;
import com.droph.useful.brms.exception.IncorrectFlowMappingException;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.ResultRule;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.Serializable;

import static com.droph.useful.brms.DecisionHelper.TestFlowType.ANOTHER_TEST_FLOW_TYPE;
import static com.droph.useful.brms.DecisionHelper.TestFlowType.TEST_FLOW_TYPE;
import static com.droph.useful.brms.DecisionHelper.getDecisionRule;
import static com.droph.useful.brms.DecisionHelper.getReasonRule;
import static com.droph.useful.brms.flow.Flow.builder;
import static com.droph.useful.brms.flow.Flow.node;
import static lombok.AccessLevel.PRIVATE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class FlowTest {
    private static final FlowType<TestResult> TEST_FLOW = TEST_FLOW_TYPE;
    private static final FlowType<TestResult> ANOTHER_TEST_FLOW = ANOTHER_TEST_FLOW_TYPE;
    @org.junit.Rule
    public ExpectedException thrown = ExpectedException.none();
    private DecisionRule<TestDecisionAware, TestResult> root;
    private DecisionRule<TestDecisionAware, TestResult> leafDecisionTrue;
    private DecisionRule<TestDecisionAware, TestResult> leafDecisionFalse;
    private ResultRule<TestDecisionAware, TestResult> leafActionTrue1;
    private ResultRule<TestDecisionAware, TestResult> leafActionFalse1;
    private ResultRule<TestDecisionAware, TestResult> leafActionTrue2;
    private ResultRule<TestDecisionAware, TestResult> leafActionFalse2;

    @Before
    public void init() {
        root = getDecisionRule("root", TestDecisionAware::isCheck_1, TEST_FLOW);
        leafDecisionTrue = getDecisionRule("leafDecisionTrue", TestDecisionAware::isCheck_2, TEST_FLOW);
        leafDecisionFalse = getDecisionRule("leafDecisionFalse", TestDecisionAware::isCheck_2, TEST_FLOW);
        leafActionTrue1 = getReasonRule("leafActionTrue1", TEST_FLOW);
        leafActionFalse1 = getReasonRule("leafActionFalse1", TEST_FLOW);
        leafActionTrue2 = getReasonRule("leafActionTrue2", TEST_FLOW);
        leafActionFalse2 = getReasonRule("leafActionFalse2", TEST_FLOW);
    }

    @Test
    public void testNode() {
        DecisionNode<TestDecisionAware, TestResult> nodeZero = node(root)
                .ifTrue(leafActionTrue1)
                .ifFalse(leafActionFalse1);
        DecisionNode<TestDecisionAware, TestResult> nodeOne = node(root, 1)
                .ifTrue(leafActionTrue2, 1)
                .ifFalse(leafActionFalse2, 1);
        assertPair(nodeZero.getRoot(), root, 0);
        assertPair(nodeOne.getRoot(), root, 1);
        assertPair(nodeZero.getTrueLeaf(), leafActionTrue1, 0);
        assertPair(nodeZero.getFalseLeaf(), leafActionFalse1, 0);
        assertPair(nodeOne.getTrueLeaf(), leafActionTrue2, 1);
        assertPair(nodeOne.getFalseLeaf(), leafActionFalse2, 1);
    }


    private void assertPair(Tuple2 pair, Object rule, int ruleNumber) {
        assertThat(pair._1, is(rule));
        assertThat(pair._2, is(ruleNumber));
    }

    @Test
    public void testBuildLoop() {
        thrown.expect(IncorrectFlowMappingException.class);
        builder(root)
                .add(node(root)
                        .ifTrue(leafDecisionTrue)
                        .ifFalse(leafDecisionFalse))
                .add(node(leafDecisionTrue)
                        .ifTrue(root)
                        .ifFalse(leafActionFalse1))
                .build();
    }

    @Test
    public void testBuildEmptyLeaf() {
        thrown.expect(IncompleteFlowException.class);
        builder(root)
                .add(node(root)
                        .ifTrue(leafDecisionTrue)
                        .ifFalse(leafDecisionFalse))
                .add(node(leafDecisionTrue)
                        .ifTrue(leafActionFalse2)
                        .ifFalse(leafActionFalse1))
                .build();
    }

    @RequiredArgsConstructor(access = PRIVATE)
    private static class TestDecisionAware implements DecisionAware {
        @Getter
        private final boolean check_1, check_2;
        private DecisionAware container = DecisionHelper.getDecisionAware(TEST_FLOW);

        public static TestDecisionAware of(boolean check_1, boolean check_2) {
            return new TestDecisionAware(check_1, check_2);
        }

        @Override
        public <R extends Serializable> void addDecision(FlowType<R> type, Decision<R> holder) {
            container.addDecision(type, holder);
        }

        @Override
        public <R extends Serializable> Option<Decision<R>> getDecision(FlowType<R> type) {
            return container.getDecision(type);
        }
    }

}
