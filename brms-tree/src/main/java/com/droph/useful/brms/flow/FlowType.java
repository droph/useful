package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.ResultRule;
import lombok.Data;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Decision Flow Type, used to bind set of rules and results to particular flow.
 * Used as unique identification of particular flow.
 *
 * @param <R> Flow result type.
 */
public interface FlowType<R extends Serializable> extends Serializable {
    /**
     * Create default implementation of FlowType based on passed params
     *
     * @param name       flow name
     * @param returnType Class type for return result of the flow
     * @param <R>        return result
     * @return Default implementation of FlowType
     */
    static <R extends Serializable> FlowType<R> of(String name, Class<R> returnType) {
        return new DefaultFlowType<>(name, returnType);
    }

    Class<R> getFlowTypeClass();

    String getName();

    /**
     * Create default implementation of result rule bind to this FlowType.
     *
     * @param name   rule name
     * @param result result which will be returned from this rule
     * @param <T>    type of object which is processed by flow
     * @return Default implementation of result rule bind to this FlowType
     */
    default <T extends DecisionAware> ResultRule<T, R> reasonRuleOf(String name, R result) {
        return ResultRule.of(name, result, this);
    }

    /**
     * Create default implementation of decision rule bind to this FlowType.
     *
     * @param name     rule name
     * @param decision function which should be applied to passed DecisionAware object
     * @param <T>      type of objects processed by flow
     * @return default implementation of DecisionRule bind to flowtype
     */

    default <T extends DecisionAware> DecisionRule<T, R> decisionRuleOf(String name, Function<T, Object> decision) {
        return DecisionRule.of(name, decision, this);
    }

    @Data
    class DefaultFlowType<R extends Serializable> implements FlowType<R> {
        private final String name;
        private final Class<R> flowTypeClass;
    }
}
