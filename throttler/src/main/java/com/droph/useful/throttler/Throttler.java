package com.droph.useful.throttler;

/**
 * @param <K> unique identifier for objects being throttled <br>
 * @param <G> grouping key <br>
 * @param <V> objects being throttled
 */
public interface Throttler<K, G, V> {
    /**
     * Add new object to throttler.
     * @param uniqueId unique Id for each object
     * @param groupingKey key by which objects are grouped. Only one object per this key will be processed by the underlying system.<br>
     *                    Other will be queued until ack() method is not explicitly called.
     * @param object object to be processed
     */
    void push(K uniqueId, G groupingKey, V object);

    void ack(K uniqueId);
}
