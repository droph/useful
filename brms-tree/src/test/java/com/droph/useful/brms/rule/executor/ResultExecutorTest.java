package com.droph.useful.brms.rule.executor;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.DecisionHelper;
import com.droph.useful.brms.DecisionHelper.TestResult;
import com.droph.useful.brms.flow.FlowType;
import com.droph.useful.brms.rule.ResultRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static com.droph.useful.brms.DecisionHelper.TestFlowType.TEST_FLOW_TYPE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ResultExecutorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ResultRule<DecisionAware, TestResult> resultRule;
    private ResultExecutor<DecisionAware, TestResult> resultExecutor;
    private FlowType<TestResult> testFlow = TEST_FLOW_TYPE;

    @Before
    public void init() {
        resultRule = DecisionHelper.getReasonRule("testRule", testFlow);
        resultExecutor = ResultExecutor.of(resultRule);
    }

    @Test
    public void testGetRule() {
        assertThat(resultExecutor.getRule(), is(resultRule));
    }

    @Test
    public void testValidate() {
        resultExecutor.validate();
    }
}
