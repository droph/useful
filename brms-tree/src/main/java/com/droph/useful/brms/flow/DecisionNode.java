package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.Rule;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.Map;

import java.io.Serializable;

public class DecisionNode<T extends DecisionAware, R extends Serializable> extends FlowNode<T, R> {
    private final SwitchNode<T, R> delegate;

    private DecisionNode(Tuple2<DecisionRule<T, R>, Integer> root) {
        this.delegate = new SwitchNode<>(root);
    }

    public static <T extends DecisionAware, R extends Serializable> DecisionNode<T, R> of(DecisionRule<T, R> rule, Integer uniqueNumber) {
        return new DecisionNode<>(Tuple.of(rule, uniqueNumber));
    }

    public DecisionNode<T, R> ifTrue(Rule<T, R> trueLeaf) {
        return ifTrue(trueLeaf, 0);
    }

    public DecisionNode<T, R> ifTrue(Rule<T, R> trueLeaf, Integer num) {
        delegate.ifCase(Boolean.TRUE).then(trueLeaf, num);
        return this;
    }

    public DecisionNode<T, R> ifFalse(Rule<T, R> falseLeaf, Integer num) {
        delegate.defaultCase(falseLeaf, num);
        return this;
    }

    public DecisionNode<T, R> ifFalse(Rule<T, R> falseLeaf) {
        return ifFalse(falseLeaf, 0);
    }

    public Tuple2<Rule<T, R>, Integer> getTrueLeaf() {
        return getRules()
                .values()
                .head();
    }

    public Tuple2<Rule<T, R>, Integer> getFalseLeaf() {
        return getDefaultRule();
    }

    @Override
    Tuple2<DecisionRule<T, R>, Integer> getRoot() {
        return delegate.getRoot();
    }

    @Override

    Map<Object, Tuple2<Rule<T, R>, Integer>> getRules() {
        return delegate.getRules();
    }

    @Override
    Tuple2<Rule<T, R>, Integer> getDefaultRule() {
        return delegate.getDefaultRule();
    }
}
