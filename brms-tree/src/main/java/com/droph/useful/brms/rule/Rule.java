package com.droph.useful.brms.rule;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.flow.FlowType;

import java.io.Serializable;

/**
 * Root interface for decision tree rules
 */
public interface Rule<T extends DecisionAware, R extends Serializable> {
    /**
     * Returns flowType for the rule
     */
    FlowType<R> getType();

    /**
     * Returns the name of the rule
     */
    String getName();
}
