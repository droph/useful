package com.okorshunov.webchat.chat

import org.springframework.data.annotation.Id

class Model {
    data class ChatMessage(val sentTo: String = "all",
                           val sentFrom: String,
                           val msgBody: String) {
        @Id
        var id: Long? = null
    }
}