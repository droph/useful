package com.droph.useful.brms.exception;

public class IncorrectFlowMappingException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IncorrectFlowMappingException(final String msg) {
        super(msg);
    }
}
