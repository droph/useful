package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.exception.IncorrectFlowMappingException;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.ResultRule;
import com.droph.useful.brms.rule.Rule;
import com.droph.useful.brms.rule.executor.DecisionExecutor;
import com.droph.useful.brms.rule.executor.ResultExecutor;
import com.droph.useful.brms.rule.executor.RuleExecutor;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Flow<T extends DecisionAware, R extends Serializable> {
    private final DecisionExecutor<T, R> initRuleExecutor;
    @Getter
    private final FlowType<R> flowType;

    public static <T extends DecisionAware, R extends Serializable> Builder<T, R> builder(DecisionRule<T, R> initRule) {
        return new Builder<>(initRule);
    }

    public static <T extends DecisionAware, R extends Serializable> DecisionNode<T, R> node(DecisionRule<T, R> initRule) {
        return node(initRule, 0);
    }

    public static <T extends DecisionAware, R extends Serializable> DecisionNode<T, R> node(DecisionRule<T, R> initRule, Integer instanceNum) {
        return DecisionNode.of(initRule, instanceNum);
    }

    public static <T extends DecisionAware, R extends Serializable> SwitchNode<T, R> switchNode(DecisionRule<T, R> rule) {
        return switchNode(rule, 0);
    }

    public static <T extends DecisionAware, R extends Serializable> SwitchNode<T, R> switchNode(DecisionRule<T, R> rule, Integer uniqueNumber) {
        return SwitchNode.of(rule, uniqueNumber);
    }

    /**
     * Process a {@link DecisionAware} through decision flow and return result
     *
     * @param decisionAware object to be processed via decision tree
     * @return {@link Decision} wrapper with flow result
     */
    public Decision<R> execute(T decisionAware) {
        log.debug("Executing decision flow () for {}", flowType, decisionAware);
        Decision.Builder<R> result = initRuleExecutor.execute(decisionAware);
        log.debug("Decision flow {} for {} executed", flowType, decisionAware);
        return result.build();
    }

    public static class Builder<T extends DecisionAware, R extends Serializable> {
        private final DecisionExecutor<T, R> rootRuleExecutor;
        private final FlowType<R> flowType;
        private Map<Tuple2<DecisionRule<T, R>, Integer>, DecisionExecutor<T, R>> decisions = new HashMap<>();
        private Map<Tuple2<ResultRule<T, R>, Integer>, ResultExecutor<T, R>> results = new HashMap<>();
        private Set<Object> nodes = new HashSet<>();

        private Builder(DecisionRule<T, R> initRule) {
            requireNonNull(initRule, "Init rule can not be null");
            requireNonNull(initRule.getType(), "Flowtype should be set for init rule");
            this.rootRuleExecutor = DecisionExecutor.of(Tuple.of(initRule, 0));
            this.flowType = initRule.getType();
            decisions.put(Tuple.of(initRule, 0), rootRuleExecutor);
        }

        /**
         * Connect node RuleExecutor with two leaf RuleExecutor objects.
         * There is not guarantee that these executors will be part of flow tree itself.
         * All nodes are validated to be part of one flow tree when build() method is called.
         */
        public Builder<T, R> add(FlowNode<T, R> node) {
            DecisionExecutor<T, R> rootExecutor = (DecisionExecutor<T, R>) getAnyTypeRuleExecutor(node.getRoot());
            checkRootExecutor(rootExecutor);
            handleRules(rootExecutor, node.getRules().toJavaMap(), node.getDefaultRule());
            return this;
        }

        public Flow<T, R> build() {
            validateFlow();
            return new Flow<>(rootRuleExecutor, flowType);
        }

        private void validateFlow() {
            checkForCycles(rootRuleExecutor);
            rootRuleExecutor.validate();
            validateNodes(rootRuleExecutor);
            if (!nodes.isEmpty()) {
                handleError("Decision RuleExecutor nodes for rule %s are not mapped to main execution flow", nodes);
            }
        }

        private void validateNodes(DecisionExecutor<T, R> ruleExecutor) {
            nodes.remove(ruleExecutor.getKey());
            ruleExecutor.getAllExecutors()
                    .filter(DecisionExecutor.class::isInstance)
                    .map(DecisionExecutor.class::cast)
                    .forEach(this::validateNodes);
        }

        private void checkForCycles(DecisionExecutor<T, R> executor) {
            checkForCycles(executor, new HashSet<>(), new HashSet<>());
        }

        private void checkForCycles(DecisionExecutor<T, R> executor,
                                    Set<DecisionExecutor<T, R>> onStack,
                                    Set<DecisionExecutor<T, R>> visited) {
            onStack.add(executor);
            visited.add(executor);
            executor.getAllExecutors()
                    .filter(DecisionExecutor.class::isInstance)
                    .map(exec -> (DecisionExecutor<T, R>) exec)
                    .forEach(exec -> {
                        if (!visited.contains(exec)) {
                            checkForCycles(exec, onStack, visited);
                        } else if (onStack.contains(exec)) {
                            handleError("Cyclic flow can not be built, Rule %s is in loop with itself", exec);
                        }
                    });
            onStack.remove(executor);
        }

        private void handleRules(DecisionExecutor<T, R> rootExecutor,
                                 Map<Object, Tuple2<Rule<T, R>, Integer>> resultBindRules,
                                 Tuple2<Rule<T, R>, Integer> defaultRule) {
            resultBindRules.entrySet().forEach(
                    resultBindRule -> {
                        checkRuleType(resultBindRule.getValue()._1);
                        rootExecutor.addCase(resultBindRule.getKey(), getAnyTypeRuleExecutor(resultBindRule.getValue()));
                    }
            );
            if (nonNull(defaultRule)) {
                checkRuleType(defaultRule._1);
                rootExecutor.addDefault(getAnyTypeRuleExecutor(defaultRule));
            }
        }

        private void checkRuleType(Rule rule) {
            if (rule.getType() != flowType) {
                handleError("Type of the Flow and Rules should match!, Flow type - %s, Rule - %s", flowType, rule);
            }
        }

        @SuppressWarnings("unchecked")
        private RuleExecutor<T, R> getAnyTypeRuleExecutor(Tuple2<? extends Rule<T, R>, Integer> pair) {
            Rule<T, R> rule = pair._1;
            if (rule instanceof ResultRule) {
                return getResultRuleExecutor((ResultRule<T, R>) pair._1, (Tuple2<ResultRule<T, R>, Integer>) pair);
            } else if (rule instanceof DecisionRule) {
                return getDecisionRuleExecutor((DecisionRule<T, R>) pair._1, (Tuple2<DecisionRule<T, R>, Integer>) pair);
            } else {
                return getRuleExecutor(pair._1, pair);
            }

        }

        private RuleExecutor<T, R> getResultRuleExecutor(ResultRule<T, R> resultRule,
                                                         Tuple2<ResultRule<T, R>, Integer> pair) {
            return results.computeIfAbsent(pair, p -> ResultExecutor.of(resultRule));
        }

        private RuleExecutor<T, R> getDecisionRuleExecutor(DecisionRule<T, R> decisionRule,
                                                           Tuple2<DecisionRule<T, R>, Integer> pair) {
            return decisions.computeIfAbsent(pair, p -> DecisionExecutor.of(pair));
        }

        private RuleExecutor<T, R> getRuleExecutor(Rule<T, R> unidentifiedRule,
                                                   Tuple2<? extends Rule<T, R>, Integer> pair) {
            throw new IncorrectFlowMappingException("Unknown rule type");
        }

        private void checkRootExecutor(DecisionExecutor<T, R> rootRuleExecutor) {
            Tuple2<DecisionRule<T, R>, Integer> key = rootRuleExecutor.getKey();
            if (nodes.contains(key)) {
                handleError("Rule %s has already been assigned with descendants", key);
            }
            nodes.add(key);
        }

        private void handleError(String format, Object... args) {
            String msg = String.format(format, args);
            log.error(msg);
            throw new IncorrectFlowMappingException(msg);
        }


    }
}
