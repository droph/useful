package com.okorshunov.webchat.prime

import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Mono
import reactor.extra.processor.TopicProcessor

class PrimeNumbersHandler : WebSocketHandler {
    private val processor = TopicProcessor.share<Event>("shared", 1024)

    override fun handle(session: WebSocketSession): Mono<Void> {
        return session
                .send(processor.map { event ->
                    session.textMessage("${event.sender}:${event.value}")
                })
                .and(session.receive()
                        .map { event ->
                            val parts = event.payloadAsText.split(":")
                            Event(sender = parts[0].toInt(), value = parts[1].toInt())
                        }
                        .filter { event -> isPrime(event.value) }
                        .doOnNext(processor::onNext))
    }

    private fun isPrime(number: Int) = number % 2 == 0
}