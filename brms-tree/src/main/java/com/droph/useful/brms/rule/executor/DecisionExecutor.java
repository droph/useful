package com.droph.useful.brms.rule.executor;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.exception.IncompleteFlowException;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.DecisionRule;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

import static java.util.Objects.isNull;

@Slf4j
@Getter
@ToString(of = "key")
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DecisionExecutor<T extends DecisionAware, R extends Serializable> implements RuleExecutor<T, R> {
    private final Tuple2<DecisionRule<T, R>, Integer> key;
    private final DecisionRule<T, R> rule;
    private Map<Object, RuleExecutor<T, R>> executorMap = HashMap.empty();
    private RuleExecutor<T, R> defaultRuleExecutor;

    public static <T extends DecisionAware, R extends Serializable> DecisionExecutor<T, R> of(Tuple2<DecisionRule<T, R>, Integer> key) {
        return new DecisionExecutor<>(key, key._1);
    }

    @Override
    public final Decision.Builder<R> execute(T decisionAware) {
        return executorMap.get(rule.execute(decisionAware))
                .getOrElse(defaultRuleExecutor)
                .execute(decisionAware)
                .prepend(rule);
    }

    public final void validate() {
        if (executorMap.isEmpty()) incompleteFlowException("Flow is not complete, %s is missing descendants");
        if (isNull(defaultRuleExecutor)) incompleteFlowException("Flow is not complete, %s must have default rule");
        executorMap.forEach((ign, executor) -> executor.validate());
        defaultRuleExecutor.validate();
    }

    public void addCase(Object result, RuleExecutor<T, R> ruleExecutor) {
        executorMap = executorMap.put(result, ruleExecutor);
    }

    public void addDefault(RuleExecutor<T, R> ruleExecutor) {
        defaultRuleExecutor = ruleExecutor;
    }

    public Seq<RuleExecutor<T, R>> getAllExecutors() {
        return executorMap.values().append(defaultRuleExecutor);
    }

    private void incompleteFlowException(String s) {
        String msg = String.format(s, this);
        log.error(msg);
        throw new IncompleteFlowException(msg);
    }

}
