package com.droph.useful.throttler;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import static junit.framework.Assert.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Slf4j
public class ConcurrentThrottlerTest {
    private ExecutorService executor = Executors.newFixedThreadPool(8);
    private Throttler<Integer, Integer, Tuple2<Integer, String>> throttler;
    private volatile int idCounter = 0;
    private Map<Integer, List<Integer>> procMap = new ConcurrentHashMap<>();
    private AtomicBoolean[] flags;

    private List<String> testList;

    @Test
    public void throttlerTest() {
        testList = new ArrayList<>();
        Throttler<Long, Integer, String> throttler = new ConcurrentThrottler<>(30000, 1000, (k, v) -> testList.add(v));
        int groupId = 1;
        long id1 = 1L;
        String obj1 = "obj1";
        long id2 = 2L;
        String obj2 = "obj2";
        long id3 = 3L;
        String obj3 = "obj3";

        throttler.push(id1, groupId, obj1);
        assertTrue(testList.size() == 1);
        assertThat(testList.get(0), is(obj1));

        throttler.push(id2, groupId, obj2);
        assertTrue(testList.size() == 1);
        assertThat(testList.get(0), is(obj1));

        throttler.push(id3, groupId, obj3);
        assertTrue(testList.size() == 1);
        assertThat(testList.get(0), is(obj1));

        throttler.ack(id1);
        assertTrue(testList.size() == 2);
        assertThat(testList.get(0), is(obj1));
        assertThat(testList.get(1), is(obj2));

        throttler.ack(id2);
        assertTrue(testList.size() == 3);
        assertThat(testList.get(2), is(obj3));

        throttler.ack(id3);
        assertTrue(testList.size() == 3);
    }

    @Test
    public void throttlerTest1() throws InterruptedException {
        int numberOfTestMessages = 1000;
        flags = new AtomicBoolean[numberOfTestMessages];

        for (int i = 0; i < numberOfTestMessages; i++) {
            flags[i] = new AtomicBoolean();
        }

        throttler = new ConcurrentThrottler<>(30000, 1000, this::testFlow);
        for (int i = 0; i < numberOfTestMessages; i++) {
            Tuple3<Integer, Integer, String> msg = consumeTestMessage();
            log.info("Throttler push: {}", msg);
            throttler.push(msg._1, msg._2, Tuple.of(msg._2, msg._3));
        }

        Thread.sleep(15000);
        procMap.forEach((id, list) -> list.stream().allMatch(e -> e == id % procMap.size()));
        assertTrue(((Map) ReflectionTestUtils.getField(throttler, "inProgress")).size() == 0);
        assertTrue(((Map) ReflectionTestUtils.getField(throttler, "groupingKeys")).size() == 0);
        assertTrue(((Map) ReflectionTestUtils.getField(throttler, "waitingQueues")).size() == 0);
        assertTrue(((Queue) ReflectionTestUtils.getField(throttler, "sizeController")).size() == 0);
    }

    private Tuple3<Integer, Integer, String> consumeTestMessage() {
        int id = idCounter++;
        int key = id % 10;
        Tuple3<Integer, Integer, String> msg = Tuple.of(id, key, "Message - " + id + " - " + key);
        log.info(msg.toString());
        return msg;

    }

    private void testFlow(Integer id, Tuple2<Integer, String> msg) {
        executor.execute(() -> {
            assertFalse("id: " + id, flags[id].getAndSet(true));
            procMap.computeIfAbsent(msg._1, k -> new ArrayList<>()).add(id);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            throttler.ack(id);
            flags[id].getAndSet(false);
        });
    }

}
