package com.okorshunov.webchat.chat

import com.okorshunov.webchat.chat.Model.ChatMessage
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/chat")
class ChatController {
    @Autowired
    lateinit var repository: ChatRepository

    @GetMapping
    fun findAll() = repository.findAll()

    @PostMapping
    fun add(@RequestParam("from") from: String,
            @RequestParam("body") body: String) = repository.save(ChatMessage(sentFrom = from, msgBody = body))
}