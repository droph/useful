package com.okorshunov.webchat.chat

import com.okorshunov.webchat.chat.Model.ChatMessage
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.extra.processor.TopicProcessor

class ChatHandler(private val repository: ChatRepository) : WebSocketHandler {
    private val processor = TopicProcessor.share<ChatMessage>("sharedChat", 1024)

    override fun handle(session: WebSocketSession): Mono<Void> {
        return session.send(Flux.concat(repository.findAll(), processor)
                                    .map { session.textMessage("${it.sentFrom}: ${it.msgBody}") })
                .and(session.receive()
                             .map { toChatMessage(it) }
                             .flatMap { repository.save(it) }
                             .doOnNext(processor::onNext))
    }

    private fun toChatMessage(webSocketMessage: WebSocketMessage): ChatMessage {
        val parts = webSocketMessage.payloadAsText.split(":")
        return ChatMessage(sentFrom = parts[0],
                           msgBody = parts[1])
    }
}