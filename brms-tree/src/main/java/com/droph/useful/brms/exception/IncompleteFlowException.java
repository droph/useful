package com.droph.useful.brms.exception;

public class IncompleteFlowException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public IncompleteFlowException(String msg) {
        super(msg);
    }
}
