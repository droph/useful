package com.okorshunov.webchat.prime

data class Event(val sender: Int, val value: Int)