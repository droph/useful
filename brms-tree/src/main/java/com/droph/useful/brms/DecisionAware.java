package com.droph.useful.brms;

import com.droph.useful.brms.flow.FlowType;
import com.droph.useful.brms.rule.Decision;
import io.vavr.control.Option;

import java.io.Serializable;

/**
 * Interface which should be implemented by the class in order to be processed by the decision flow.
 */
public interface DecisionAware {
    /**
     * Add decision result into the container for the paricular FlowType
     *
     * @param type   type of the flow, which processed the object instance
     * @param holder decision result holder
     * @param <R>    result type
     */
    <R extends Serializable> void addDecision(FlowType<R> type, Decision<R> holder);

    /**
     * Returns decision holder for FlowType
     *
     * @param type type of the flow
     * @param <R>  result type
     * @return Option of Decision holder
     */
    <R extends Serializable> Option<Decision<R>> getDecision(FlowType<R> type);
}
