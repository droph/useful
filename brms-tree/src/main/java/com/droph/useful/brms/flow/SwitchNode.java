package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.Rule;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class SwitchNode<T extends DecisionAware, R extends Serializable> extends FlowNode<T, R> {
    private final Tuple2<DecisionRule<T, R>, Integer> root;
    private Map<Object, Tuple2<Rule<T, R>, Integer>> rules = HashMap.empty();
    private Tuple2<Rule<T, R>, Integer> defaultRule;

    public static <T extends DecisionAware, R extends Serializable> SwitchNode<T, R> of(DecisionRule<T, R> rule, Integer uniqueNumber) {
        return new SwitchNode<>(Tuple.of(rule, uniqueNumber));
    }

    public CaseThen ifCase(Object result) {
        return new CaseThen(result);
    }

    public SwitchNode<T, R> defaultCase(Rule<T, R> rule, Integer uniqueNumber) {
        defaultRule = Tuple.of(rule, uniqueNumber);
        return this;
    }

    public SwitchNode<T, R> defaultCase(Rule<T, R> rule) {
        return defaultCase(rule, 0);
    }

    @RequiredArgsConstructor
    public class CaseThen {
        private final Object result;

        public SwitchNode<T, R> then(Rule<T, R> rule, Integer uniqueNumber) {
            rules = rules.put(result, Tuple.of(rule, uniqueNumber));
            return SwitchNode.this;
        }

        public SwitchNode<T, R> then(Rule<T, R> rule) {
            return then(rule, 0);
        }
    }
}
