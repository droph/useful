package com.droph.useful.brms.rule.executor;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.Rule;

import java.io.Serializable;

/**
 * Root interface for rule executors.
 * Executors are wrappers on Rules which provides API to build and execute them in the flow.
 *
 * @param <T> Type of objects processed by the rules
 * @param <R> Result type of the flow
 */
public interface RuleExecutor<T extends DecisionAware, R extends Serializable> {
    Rule<T, R> getRule();

    Decision.Builder<R> execute(T decisionAware);

    default void validate() {
        //Default implementation implies that rule is the end of the execution flow hence nothing is done here.
    }
}
