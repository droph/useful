package com.okorshunov.webchat

import com.okorshunov.webchat.chat.ChatHandler
import com.okorshunov.webchat.chat.ChatRepository
import com.okorshunov.webchat.chat.DatabaseCreator
import com.okorshunov.webchat.prime.PrimeNumbersHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.core.DatabaseClient
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter

@Configuration
@EnableWebFlux
@EnableTransactionManagement
class WebSocketConfig {

    @Bean
    fun webSocketHandlerAdapter() = WebSocketHandlerAdapter()

    @Bean
    fun primeNumbersHandler() = PrimeNumbersHandler()

    @Bean
    fun handlerMapping(primeNumbersHandler: PrimeNumbersHandler,
                       chatHandler: ChatHandler): HandlerMapping {
        val handlerMapping = SimpleUrlHandlerMapping()
        handlerMapping.urlMap = mapOf("ws/primes" to primeNumbersHandler,
                                      "ws/chat" to chatHandler)
        handlerMapping.order = 1
        return handlerMapping
    }

    @Bean
    fun chatHandler(repository: ChatRepository): ChatHandler {
        return ChatHandler(repository)
    }

    @Bean(initMethod = "initDb")
    fun databaseCreator(client: DatabaseClient): DatabaseCreator {
        return DatabaseCreator(client)
    }

}