package com.droph.useful.brms.rule;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.flow.FlowType;
import lombok.Data;

import java.io.Serializable;

/**
 * Interface for Result Rules. Each implementation should override equals and hashCode methods from Object class to guarantee correct processing of objects by the rule and flow.
 * Static factory method of() should be used to create instances with required methods implemented.
 *
 * @param <T> type of objects processed by the rule and flow
 * @param <R> result type
 */
public interface ResultRule<T extends DecisionAware, R extends Serializable> extends Rule<T, R> {
    /**
     * Create default implementation of ResultRule based on passed parameters
     *
     * @param name     rule name
     * @param result   result of the rule
     * @param flowType flowType bind to rule
     * @param <T>      type of objects processed by the flow
     * @param <R>      result type
     * @return Default implementation of ResultRule based on passed parameters
     */
    static <T extends DecisionAware, R extends Serializable> ResultRule<T, R> of(String name, R result, FlowType<R> flowType) {
        return new DefaultResultRule<>(flowType, name, result);
    }

    /**
     * Returns result for the rule
     */
    R getResult();

    @Data
    class DefaultResultRule<T extends DecisionAware, R extends Serializable> implements ResultRule<T, R> {
        private final FlowType<R> type;
        private final String name;
        private final R result;
    }
}
