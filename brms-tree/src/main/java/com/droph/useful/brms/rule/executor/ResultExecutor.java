package com.droph.useful.brms.rule.executor;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.Decision;
import com.droph.useful.brms.rule.ResultRule;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * Wrapper on ResultRule in order to execute it in the Flow.
 * {@inheritDoc}
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class ResultExecutor<T extends DecisionAware, R extends Serializable> implements RuleExecutor<T, R> {
    private final ResultRule<T, R> rule;

    public static <T extends DecisionAware, R extends Serializable> ResultExecutor<T, R> of(ResultRule<T, R> resultRule) {
        return new ResultExecutor<>(resultRule);
    }

    @Override
    public Decision.Builder<R> execute(T decisionAware) {
        return Decision.builder(rule);
    }
}
