package com.droph.useful.brms.flow;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.rule.DecisionRule;
import com.droph.useful.brms.rule.Rule;
import io.vavr.Tuple2;
import io.vavr.collection.Map;

import java.io.Serializable;

/**
 * Class to define a node of the flow.
 * Abstract class is used instead of the Interface to make methods package private
 * @param <T> Type of objects processed by the Flow
 * @param <R> result type
 */
abstract class FlowNode<T extends DecisionAware, R extends Serializable> {
    abstract Tuple2<DecisionRule<T, R>, Integer> getRoot();

    abstract Map<Object, Tuple2<Rule<T, R>, Integer>> getRules();

    abstract Tuple2<Rule<T, R>, Integer> getDefaultRule();
}
