package com.droph.useful.brms.flow.graph;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.flow.Flow;
import com.droph.useful.brms.rule.executor.DecisionExecutor;
import com.droph.useful.brms.rule.executor.ResultExecutor;
import com.droph.useful.brms.rule.executor.RuleExecutor;
import io.vavr.Tuple2;
import io.vavr.collection.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.DefaultGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import java.io.Serializable;
import java.util.function.*;

import static java.lang.Math.max;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

public class FlowGraphBuilder {
    public static final String ACTION_NODE_STYLE = "fill-color; green;";
    public static final String DECISION_NODE_STYLE = "fill-color; white;";
    public static final String INIT_NODE_STYLE = "fill-color; cyan;";
    public static final String LEVEL = "level";
    private static final Function<RuleExecutor, String> DEFAULT_NAME_FN = ruleExecutor -> ruleExecutor.getRule().getName();
    private static final BiFunction<DecisionExecutor, Integer, String> DECISION_RULE_NAME_FN = (de, x) ->
            (x > 0 ? DEFAULT_NAME_FN.andThen(n -> n + " - " + x) : DEFAULT_NAME_FN).apply(de);

    private FlowGraphBuilder() {
        throw new AssertionError();
    }

    @SneakyThrows
    public static ViewPanel displayFlow(Flow flow) {
        DecisionExecutor initExecutor = (DecisionExecutor) FieldUtils.readField(flow, "initRuleExecutor", true);
        Viewer viewer = new Viewer(buildGraph(flow.getFlowType().getName(), initExecutor), Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
        return viewer.addView(Viewer.DEFAULT_VIEW_ID, Viewer.newGraphRenderer());
    }

    private static Graph buildGraph(String name, DecisionExecutor ruleExecutor) {
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        Graph graph = new DefaultGraph(name, false, false);
        graph.addAttribute("ui.quality");
        graph.addAttribute("ui.antialias");
        graph.addAttribute("ui.stylesheet", getDefaultGraphStyle());
        Node initNode = addNodes(graph, ruleExecutor);
        addEdges(graph, ruleExecutor);
        locateNodes(graph, initNode);
        return graph;
    }

    private static void locateNodes(Graph graph, Node node) {
        requireNonNull(graph);
        requireNonNull(node);

        markNodeLevel(node, 0);

        Map<Integer, List<Node>> levels = List.<Node>ofAll(graph.getNodeSet())
                .groupBy(n -> n.getAttribute(LEVEL));

        int levelsNumber = levels.size();
        double yScale = 1. / (levelsNumber + 1);

        levels.forEach(level -> {
                    double xScale = 1. / (level._2.size() + 1);
                    for (int j = 0; j < level._2.size(); j++) {
                        level._2.get(j).setAttribute("xy", 1 - (j + 1) * xScale, 1 - (level._1 + 1) * yScale);
                    }
                }
        );

    }

    private static void markNodeLevel(Node node, int level) {
        Integer prevLevel = node.getAttribute(LEVEL);
        node.addAttribute(LEVEL, isNull(prevLevel) ? level : max(prevLevel, level));
        node.getLeavingEdgeSet().stream()
                .map(Edge::getTargetNode)
                .forEach(n -> markNodeLevel((Node) n, level + 1));
    }

    private static <T extends DecisionAware, R extends Serializable> void addEdges(Graph graph, DecisionExecutor<T, R> decisionExecutor) {
        getAllEdges(decisionExecutor).forEach(edge -> addEdge(graph, edge));
    }

    private static Edge addEdge(Graph graph, FlowEdge flowEdge) {
        Edge edge = graph.addEdge(flowEdge.getId(), flowEdge.getSourceNodeId(), flowEdge.getTargetNodeId(), true);
        edge.addAttribute("ui.label", flowEdge);
        return edge;
    }

    private static <T extends DecisionAware, R extends Serializable> Seq<FlowEdge> getAllEdges(DecisionExecutor<T, R> decisionExecutor) {
        return getEdgesInner(List.empty(), decisionExecutor);
    }

    private static <T extends DecisionAware, R extends Serializable> List<FlowEdge> getEdgesInner(List<FlowEdge> acc, DecisionExecutor<T, R> executor) {
        BinaryOperator<FlowEdge> flowEdgeCombiner = (fe1, fe2) -> fe1.mapName(name -> name + ", " + fe2.name);

        Map<Object, RuleExecutor<T, R>> executors = executor.getExecutorMap()
                .put("default", executor.getDefaultRuleExecutor());

        Seq<FlowEdge> edges = executors
                .map(t -> FlowEdge.of(executor, t))
                .groupBy(FlowEdge::getId)
                .mapValues(seq -> seq.reduce(flowEdgeCombiner))
                .values();

        return executors.values()
                .filter(DecisionExecutor.class::isInstance)
                .map(re -> getEdgesInner(acc, (DecisionExecutor<T, R>) re))
                .foldLeft(acc.appendAll(edges), List::appendAll);
    }

    private static <T extends DecisionAware, R extends Serializable> Node addNodes(Graph graph,
                                                                                   DecisionExecutor<T, R> decisionExecutor) {
        getAllExecutors(decisionExecutor)
                .forEach(executor -> {
                    if (ResultExecutor.class.isInstance(executor)) {
                        addNode(graph, executor, ACTION_NODE_STYLE, () -> DEFAULT_NAME_FN.apply(executor));
                    } else {
                        addNode(graph, executor, DECISION_NODE_STYLE, () -> DECISION_RULE_NAME_FN.apply((DecisionExecutor<T, R>) executor, ((DecisionExecutor<T, R>) executor).getKey()._2));
                    }
                });
        return addNode(graph, decisionExecutor, INIT_NODE_STYLE, () -> DECISION_RULE_NAME_FN.apply(decisionExecutor, decisionExecutor.getKey()._2));
    }

    private static Node addNode(Graph graph, Object obj, String style, Supplier<String> name) {
        Node node = graph.addNode(obj.toString());
        node.addAttribute("ui.label", name.get());
        node.addAttribute("ui.style", style);
        return node;
    }

    private static <T extends DecisionAware, R extends Serializable> Set<RuleExecutor<T, R>> getAllExecutors(DecisionExecutor<T, R> decisionExecutor) {
        return getExecutorsInner(HashSet.empty(), decisionExecutor);
    }

    @SuppressWarnings("unchecked")
    private static <T extends DecisionAware, R extends Serializable> Set<RuleExecutor<T, R>> getExecutorsInner(Set<RuleExecutor<T, R>> acc, RuleExecutor<T, R> executor) {
        if (ResultExecutor.class.isInstance(executor)) {
            return acc.add(executor);
        } else if (DecisionExecutor.class.isInstance(executor)) {
            ((DecisionExecutor) executor).getAllExecutors()
                    .map(ex -> getExecutorsInner(acc, (RuleExecutor) ex))
                    .forEach(ex -> acc.add((RuleExecutor) ex));

            return acc;
        } else {
            throw new UnsupportedOperationException();
        }

    }

    private static String getDefaultGraphStyle() {
        return "node {" +
                "shape: rounded-box;" +
                "padding: 5px;" +
                "stroke-mode: plain;" +
                "size-mode: fit;" +
                "text-style: bold;" +
                "}" +
                "edge {" +
                "arrow-size: 15px, 5px;" +
                "text-size: 15;" +
                "fill-color: black;" +
                "}";
    }

    @Getter
    @RequiredArgsConstructor
    public static class FlowEdge {
        private final String id;
        private final String name;
        private final String sourceNodeId;
        private final String targetNodeId;

        static FlowEdge of(String id, String name, String sourceNodeId, String targetNodeId) {
            return new FlowEdge(id, name, sourceNodeId, targetNodeId);
        }

        static <T extends DecisionAware, R extends Serializable> FlowEdge of(RuleExecutor source, Tuple2<Object, RuleExecutor<T, R>> target) {
            String sourceId = source.toString();
            String targetId = target._2.toString();
            return of(sourceId + targetId, target._1.toString(), sourceId, targetId);
        }

        FlowEdge mapName(UnaryOperator<String> mapper) {
            return of(id, mapper.apply(name), sourceNodeId, targetNodeId);
        }
    }

}
