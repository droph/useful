package com.droph.useful.brms.rule;

import com.droph.useful.brms.DecisionAware;
import com.droph.useful.brms.flow.FlowType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.function.Function;

/**
 * Interface for decision rules
 */
public interface DecisionRule<T extends DecisionAware, R extends Serializable> extends Rule<T, R> {
    /**
     * Creates default implementation of Decision Rule based on parameters passed
     *
     * @param name     rule name
     * @param decision function which should be applied to passed object
     * @param flowType flowType bind to the rule
     * @param <T>      objects proccess
     * @param <R>
     * @return
     */
    static <T extends DecisionAware, R extends Serializable> DecisionRule<T, R> of(String name, Function<T, Object> decision, FlowType<R> flowType) {
        return new DefaultDecisionRule<>(name, decision, flowType);
    }

    /**
     * Returns result for passed object
     */
    Object execute(T decisionAware);

    @Data
    class DefaultDecisionRule<T extends DecisionAware, R extends Serializable> implements DecisionRule<T, R> {
        private final String name;
        @Getter(AccessLevel.NONE)
        private final Function<T, Object> decision;
        private final FlowType<R> type;

        @Override
        public Object execute(T decisionAware) {
            return decision.apply(decisionAware);
        }
    }
}
