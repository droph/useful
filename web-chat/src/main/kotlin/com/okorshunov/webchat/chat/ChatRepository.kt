package com.okorshunov.webchat.chat

import com.okorshunov.webchat.chat.Model.ChatMessage
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface ChatRepository : ReactiveCrudRepository<ChatMessage, Long> {
}