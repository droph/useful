package com.droph.useful.throttler;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Queue;
import java.util.concurrent.*;
import java.util.function.BiConsumer;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Concurrent implementation of throtting mechanism.
 * {@inheritDoc}
 *
 * @param <K>
 * @param <G>
 * @param <V>
 */
@Slf4j
@RequiredArgsConstructor
public class ConcurrentThrottler<K, G, V> implements Throttler<K, G, V> {
    private static final Object PRESENT = new Object();
    private final long timeout;
    private final BiConsumer<K, V> objectProcessor;
    private final ConcurrentMap<G, Object> inProgress = new ConcurrentHashMap<>();
    private final ConcurrentMap<G, Queue<Tuple2<K, V>>> waitingQueues = new ConcurrentHashMap<>();
    private final ConcurrentMap<K, G> groupingKeys = new ConcurrentHashMap<>();
    private final BlockingQueue<Object> sizeController;

    public ConcurrentThrottler(int sizeLimit, long timeout, BiConsumer<K, V> objectProcessor) {
        this(timeout, objectProcessor, new LinkedBlockingQueue<>(sizeLimit));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void push(K uniqueId, G groupingKey, V object) {
        groupingKeys.put(uniqueId, groupingKey);
        if (isNull(inProgress.computeIfPresent(groupingKey, (key, val) -> putToWaiting(uniqueId, key, object)))) {
            log.debug("No entity found currently processing for key{}, message sent for processing", groupingKey);
            inProgress.put(groupingKey, PRESENT);
            objectProcessor.accept(uniqueId, object);
        }
    }

    private Object putToWaiting(K uniqueId, G key, V object) {
        waitingQueues.compute(key, (k, queue) -> {
            Queue<Tuple2<K, V>> newQueue = isNull(queue) ? new ConcurrentLinkedQueue<>() : queue;
            newQueue.add(Tuple.of(uniqueId, object));
            return newQueue;
        });
        incrementWaitBufferSize();
        return PRESENT;
    }

    @SneakyThrows
    private void incrementWaitBufferSize() {
        if (!sizeController.offer(PRESENT, timeout, MILLISECONDS)) {
            throw new IllegalStateException("Throttler has no free space after timeout expired");
        }
        log.debug("Object was added to the waiting queue, queue size = {}", sizeController.size());
    }

    @Override
    public void ack(K uniqueId) {
        log.debug("Acking id: {}", uniqueId);
        G key = requireNonNull(groupingKeys.remove(uniqueId), () -> "Not found key for id: " + uniqueId);
        inProgress.compute(key, (inProgressKey, inProgressOldValue) -> {
            Queue<Tuple2<K, V>> newQueue = waitingQueues.compute(inProgressKey, (waitKey, oldQueue) -> sendFromQueueForProcessing(oldQueue));
            return isNull(newQueue) ? null : inProgressOldValue;
        });
    }

    private Queue<Tuple2<K, V>> sendFromQueueForProcessing(Queue<Tuple2<K, V>> oldQueue) {
        return Option.of(oldQueue)
                .peek(queue -> {
                    Tuple2<K, V> tuple = queue.poll();
                    decrementWaitBufferSize();
                    objectProcessor.accept(tuple._1, tuple._2);
                })
                .filter(queue -> !queue.isEmpty())
                .getOrNull();
    }

    private void decrementWaitBufferSize() {
        sizeController.poll();
        log.debug("Object was sent for processing from waiting cache, size = {}", sizeController.size());
    }
}
