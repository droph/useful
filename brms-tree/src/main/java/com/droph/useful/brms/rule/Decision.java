package com.droph.useful.brms.rule;

import io.vavr.collection.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;

@Slf4j
@Getter
@ToString
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class Decision<R extends Serializable> implements Serializable {
    private static final long serialVersionUID = 1L;
    private final R result;
    private final String path;

    public static <R extends Serializable> Builder<R> builder(ResultRule<?, R> result) {
        return Builder.of(result);
    }

    public static class Builder<R extends Serializable> {
        private static final String PATH_DELIMITER = "/";
        private final R result;
        private List<Rule> rules;

        private Builder(ResultRule<?, R> rule) {
            Validate.notNull(rule, "Rule should be not null");
            result = rule.getResult();
            this.rules = List.of(rule);
        }

        private static <R extends Serializable> Builder<R> of(ResultRule<?, R> rule) {
            return new Builder<>(rule);
        }

        public Builder<R> prepend(Rule rule) {
            rules = rules.prepend(rule);
            return this;
        }

        public Decision<R> build() {
            String completePath = rules.map(Rule::getName)
                    .mkString(PATH_DELIMITER);
            return new Decision<>(result, completePath);
        }
    }
}
